variable "main_cidr_block" {
  default = ""
}
module "vpc" {
  source = "./modules/vpc"
  main_cidr_block = var.main_cidr_block
}

module "subnet" {
  source = "./modules/subnet"

  //output del module
  vpc_id = module.vpc.vpc_id
  cidr = sidersubnet(module.vpc)
}