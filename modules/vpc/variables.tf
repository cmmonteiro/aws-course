variable "vpc_cidr_block" {
  type ="string"
  description = "string"
}

variable "main_cidr_block" {
  default = "10.111.1.0/24"
}